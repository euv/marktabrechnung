<?php

namespace app\controllers;

use Da\User\Filter\AccessRuleFilter;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Url;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRuleFilter::class,
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionGebuehrensatzung()
    {
        $url = Url::to('@web/pdf/Gebuehrensatzung_Wochenmaerkte_2017.pdf');

        return $this->render('index', [
            'url' => $url,
            'title' => 'Gebührensatzung'
        ]);
    }

    public function actionDurchfuehrung()
    {
        $url = Url::to('@web/pdf/Maerkte_-_Durchfuehrung_von_Wochenmaerkten_2017_bis_2019.pdf');

        return $this->render('index', [
            'url' => $url,
            'title' => 'Durchführung'
        ]);
    }

    public function actionMarktsatzung()
    {
        $url = Url::to('@web/pdf/Marktsatzung.pdf');

        return $this->render('index', [
            'url' => $url,
            'title' => 'Marktsatzung'
        ]);
    }

    public function actionWochenmarktverordnung()
    {
        $url = Url::to('@web/pdf/Wochenmarktverordnung.pdf');

        return $this->render('index', [
            'url' => $url,
            'title' => 'Wochenmarktverordnung'
        ]);
    }
}
