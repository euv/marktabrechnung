<?php

namespace app\controllers;

use Yii;
use app\models\Markt;
use app\models\MarktSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Da\User\Filter\AccessRuleFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\data\ArrayDataProvider;

/**
 * MarktController implements the CRUD actions for Markt model.
 */
class MarktController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRuleFilter::class,
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['fibu'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['editor'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Markt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Markt::find()->where(['status' => 1]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Markt models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $searchModel = new MarktSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Markt model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Markt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Markt();
        $model->status = 1;

        $postData = Yii::$app->request->post();

        if ($postData && isset($postData['Markt']['MarkttageCheckboxes']) && $postData['Markt']['MarkttageCheckboxes']) {
            $postData['Markt']['Markttage'] = implode(',', $postData['Markt']['MarkttageCheckboxes']);
        }

        if ($model->load($postData) && $model->save()) {
            return $this->redirect(['admin']);
        }

        $model->MarkttageCheckboxes = explode(',', $model->Markttage);

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Markt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $postData = Yii::$app->request->post();

        if ($postData && isset($postData['Markt']['MarkttageCheckboxes'])) {
            $postData['Markt']['Markttage'] = implode(',', $postData['Markt']['MarkttageCheckboxes']);
        }

        if ($model->load($postData) && $model->save()) {
            return $this->redirect(['admin']);
        }

        $model->MarkttageCheckboxes = explode(',', $model->Markttage);

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        if ($model = $this->findModel($id)) {
            $model->status = 3;
            if ($model->save()) {
                return $this->redirect(['admin']);
            }
        }
    }

    protected function findModelsByDayOfWeek($dayNr)
    {
        if ($models = Markt::find()->where(['like', 'Markttage', '%'.$dayNr.'%', false])->all()){
           $provider = new ArrayDataProvider([
               'key' => 'id',
               'allModels' => $models,
               'sort' => [
                   'attributes' => ['id'],
               ],
               'pagination' => [
                   'pageSize' => 10,
               ],
           ]);
           return  ListView::widget([
               'dataProvider' => $provider,
               'summary' => '',
               'itemOptions' => ['class' => 'item'],
               'itemView' => function ($model, $key, $index, $widget) {
                   return '<p>'.Html::a(
                           Html::encode($model->getButtonLabel()),
                           'javascript:void(0)',
                           [
                               'class' => $model->getButtonStyle(),
                               'onclick' => 'setMarketId(this)',
                               'data' => ['marktid' => $model->MarktID]]).'</p>';
               },
           ]);
        }
        return '';
    }

    /**
     * Finds the Markt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Markt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Markt::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
