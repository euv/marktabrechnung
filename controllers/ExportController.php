<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\ExportSearchObj;
use app\models\ExportSearchVeranl;
use Da\User\Filter\AccessRuleFilter;

/**
 * KundeController implements the CRUD actions for Kunde model.
 */
class ExportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRuleFilter::class,
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['fibu'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModelObj = new ExportSearchObj();
        $searchModelVeranl = new ExportSearchVeranl();

        $dataProviderObj = $searchModelObj->search(Yii::$app->request->queryParams);
        $dataProviderVeranl = $searchModelVeranl->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModelObj' => $searchModelObj,
            'searchModelVeranl' => $searchModelVeranl,
            'dataProviderObj' => $dataProviderObj,
            'dataProviderVeranl' => $dataProviderVeranl,
        ]);
    }

}
