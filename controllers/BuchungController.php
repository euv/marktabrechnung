<?php

namespace app\controllers;

use app\models\KundeSearch;
use Da\User\Filter\AccessRuleFilter;
use Yii;
use app\models\Buchung;
use app\models\Markt;
use app\models\Kunde;
use app\models\BuchungSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * BuchungController implements the CRUD actions for Buchung model.
 */
class BuchungController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRuleFilter::class,
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['fibu'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'index-filtered',
                            'view-filtered',
                            'create-filtered',
                            'create-kunde-filtered',
                            'update-kunde-filtered',
                            'buche-filtered',
                            'delete'
                        ],
                        'roles' => ['editor'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Buchung models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BuchungSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->checkIsFirstBooking();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexFiltered($marktid, $date)
    {
        $searchModel = new BuchungSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-filtered', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => ['marktid' => $marktid, 'date' => $date]
        ]);
    }

    public function actionViewFiltered($id, $marktid, $date)
    {
        return $this->render('view-filtered', [
            'model' => $this->findModel($id),
            'params' => ['marktid' => $marktid, 'date' => $date]
        ]);
    }

    public function actionCreateFiltered($marktid, $date)
    {
        $searchModel = new KundeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('create-filtered', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => ['marktid' => $marktid, 'date' => $date]
        ]);

    }

    public function actionCreateKundeFiltered($marktid, $date)
    {
        $model = new Kunde();

        $model->status = 1;
        $model->Land = "Deutschland";

        $model->KundenID = $model->lastId + 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['buche-filtered', 'marktid' => $marktid, 'date' => $date, 'kunde' => $model->KundenID]);
        }

        return $this->render('create-kunde-filtered', [
            'model' => $model,
            'params' => ['marktid' => $marktid, 'date' => $date]
        ]);

    }

    public function actionUpdateKundeFiltered($marktid, $date, $kunde)
    {
        $model = Kunde::find()->where(['KundenID' => $kunde])->one();

        $postData = Yii::$app->request->post();

        if(isset($postData) && $postData){
            $postData['Kunde']['modified_at'] = date('Y-m-d H:i:s');
        }

        if ($model->load($postData) && $model->save()) {
            return $this->redirect(['create-filtered', 'marktid' => $marktid, 'date' => $date]);
        }

        return $this->render('create-kunde-filtered', [
            'model' => $model,
            'params' => ['marktid' => $marktid, 'date' => $date]
        ]);
    }

    public function actionBucheFiltered($marktid, $date, $kunde)
    {
        $model = new Buchung();
        $model->status = 1;

        $markt = Markt::find()->where(['MarktID'=>$marktid])->one();
        $kunde = Kunde::find()->where(['KundenID'=>$kunde])->one();
        $model->MarktId = $markt->id;
        $model->Debitor = $kunde->id;
        $model->Datum = $date;

        $postData = Yii::$app->request->post();

        if (isset($postData['Buchung']['Datum']) && $postData['Buchung']['Datum']){
            $time = strtotime($postData['Buchung']['Datum']);
            $postData['Buchung']['Datum'] = date('Y-m-d H:i:s', $time);
        }

        $this->checkIsFirstBooking();
        $model = $this->setFirstBooking($model);

        if ($model->load($postData) && $model->save()) {
            return $this->redirect(['create-filtered', 'marktid' => $marktid, 'date' => $date]);
        }

        return $this->render('buche-filtered', [
            'model' => $model,
            'markt' => $markt,
            'kunde' => $kunde,
            'params' => ['marktid' => $marktid, 'date' => $date, 'kunde' => $kunde]
        ]);

    }

    /**
     * Displays a single Buchung model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Buchung model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Buchung();
        $model->status = 1;

        $postData = Yii::$app->request->post();

        if (isset($postData['Buchung']['Datum']) && $postData['Buchung']['Datum']){
            $time = strtotime($postData['Buchung']['Datum']);
            $postData['Buchung']['Datum'] = date('Y-m-d H:i:s', $time);
        }

        $this->checkIsFirstBooking();
        $model = $this->setFirstBooking($model);

        if ($model->load($postData) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Buchung model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */


    /**
     * Deletes an existing Buchung model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $marktid='', $date='')
    {
        if ($model = $this->findModel($id)) {
            $model->scenario = 'delete';
            $model->status = 3;
            if ($model->save()) {
                if ($marktid && $date){
                    return $this->redirect(['index-filtered','marktid'=>$marktid,'date'=>$date]);
                } else {
                    return $this->redirect(['index']);
                }
            }
        }
    }

    /**
     * Finds the Buchung model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Buchung the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Buchung::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function setFirstBooking($model) {
        $test = Buchung::find()
            ->where(['Debitor' => $model->Debitor, 'MarktId' => $model->MarktId])
            ->andWhere(['not', ['buchung.status' => 3]])->one();

        $model->isFirstBooking = 0;
        if ($test === null) {
            $model->isFirstBooking = 1;
        }
        return $model;
    }

    private function checkIsFirstBooking() {

        // find and update 1st bookings = 1 with status = 3 (disabled) -> set 0 -> find next booking with status != 3 -> set 1
        $findDisabled = Buchung::find()
            ->where(['isFirstBooking' => 1])
            ->andWhere(['status' => 3])
            ->orderBy(['Datum'=>SORT_ASC])
            ->all();
        if (count($findDisabled) > 0){
            foreach($findDisabled as $model) {
                $model->isFirstBooking = 0;
                $model->save();
                $findNext = Buchung::find()
                    ->where(['Debitor' => $model->Debitor, 'MarktId' => $model->MarktId])
                    ->andWhere(['not', ['status' => 3]])
                    ->orderBy(['Datum'=>SORT_ASC])
                    ->one();
                if ($findNext !== null){
                    $findNext->isFirstBooking = 1;
                    $findNext->save();
                }
            }
        }

        // optional: reverse above if status changes back from 3->1, but this is not possible in the backend for now

        // find and update NULL entries
        $test = Buchung::find()
            ->where(['isFirstBooking' => NULL])
            ->orderBy(['Datum'=>SORT_ASC])
            ->all();
        if (count($test) > 0){
            $findArr = [];
            foreach($test as $model) {
                if (!isset($findArr[$model->Debitor])) {
                    $findArr[$model->Debitor] = [];
                }

                $model->isFirstBooking = 0;
                if (!in_array($model->MarktId, $findArr[$model->Debitor]) && $model->status !== 3) {
                    $findArr[$model->Debitor][] = $model->MarktId;
                    $model->isFirstBooking = 1;
                }
                $model->save();
            }
        }
    }

}
