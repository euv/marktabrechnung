<?php

/* @var $this yii\web\View */

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => 'Markt', 'url' => ['/markt/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="body-content">

        <?= \yii2assets\pdfjs\PdfJs::widget([
          'width'=>'100%',
          'height'=> '600px',
          'url'=> $url
        ]); ?>

    </div>
</div>
