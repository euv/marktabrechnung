<?php

use yii\helpers\Html;
use app\overrides\kartikv\yii2export\src\ExportMenu;


/* @var $this yii\web\View */
/* @var $searchModelObj app\models\ExportSearchObj */
/* @var $searchModelVeranl app\models\ExportSearchObj */
/* @var $dataProviderObj yii\data\ActiveDataProvider */
/* @var $dataProviderVeranl yii\data\ActiveDataProvider */

$this->title = 'Export';
$this->params['breadcrumbs'][] = $this->title;

$gridColumnsObj = [
    'debitorNr',
    'objTitle',
    'zusatzInfo',
    'objBeginn',
];

$gridColumnsVeranl = [
    'zusatzInfo',
    'menge',
    'veranlBeginn',
    'veranlEnde',
    [
        'attribute' => 'kategorie',
        'filter'    => $searchModelVeranl->KategorieList
    ],
];

?>

<div class="export-index">
    <h1><?= Html::encode('Export - EUV Objekte anlegen') ?></h1>
    <?php

    echo \kartik\grid\GridView::widget([
        'dataProvider' => $dataProviderObj,
        'filterModel' => $searchModelObj,
        'columns' => $gridColumnsObj
    ]);

    echo ExportMenu::widget([
        'dataProvider' => $dataProviderObj,
        'filterModel' => $searchModelObj,
        'columns' => $gridColumnsObj,
        'showConfirmAlert' => false,
        'target' => ExportMenu::TARGET_SELF,
        'asDropdown' => false,
        'clearBuffers' => true,
        'encoding' => 'ISO-8859-1',
        'filename' => 'EUV_Objekte_anlegen',
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_PDF => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_EXCEL_X => false,
            ExportMenu::FORMAT_CSV => [
                'label' => 'CSV Export',
                'iconOptions' => ['class' => 'btn-primary'],
                'linkOptions' => ['class' => 'btn btn-primary'],
                'mime' => 'application/csv',
                'extension' => 'txt',
                'delimiter' => ";",
                'writer' => ExportMenu::FORMAT_CSV,
                'options' => ['class' => 'list-unstyled'],
            ],
        ]
    ]);
    ?>
    <hr>
    <h1><?= Html::encode('Export - EUV Veranlangung erzeugen') ?></h1>
    <?php

    echo \kartik\grid\GridView::widget([
        'dataProvider' => $dataProviderVeranl,
        'filterModel' => $searchModelVeranl,
        'columns' => $gridColumnsVeranl
    ]);

    echo ExportMenu::widget([
        'dataProvider' => $dataProviderVeranl,
        'filterModel' => $searchModelVeranl,
        'columns' => $gridColumnsVeranl,
        'showConfirmAlert' => false,
        'target' => ExportMenu::TARGET_SELF,
        'asDropdown' => false,
        'clearBuffers' => true,
        'encoding' => 'ISO-8859-1',
        'filename' => 'EUV_Veranlagungen_erzeugen',
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_PDF => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_EXCEL_X => false,
            ExportMenu::FORMAT_CSV => [
                'label' => 'CSV Export',
                'iconOptions' => ['class' => 'btn-primary'],
                'linkOptions' => ['class' => 'btn btn-primary'],
                'mime' => 'application/csv',
                'extension' => 'txt',
                'delimiter' => ";",
                'writer' => ExportMenu::FORMAT_CSV,
                'options' => ['class' => 'list-unstyled'],
            ],
        ]
    ]);
    ?>
</div>
