<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MarktSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Märkte';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="markt-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'Bezeichnung',
                'filter'    => $searchModel->MarktList
            ],
            [
                'attribute' => '_MarktID',
                'filter'    => $searchModel->MarktIDList
            ],
            '_Markttage',
            [
                'attribute' => 'Kostenstelle',
                'filter'    => $searchModel->KostenstelleList
            ],
            [
                'attribute' => 'Kostenträger',
                'filter'    => $searchModel->KostenträgerList
            ],
            [
                'attribute' => '_status',
                'filter'    => $searchModel->StatusList
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons'=>[
                    'update'=>function ($url, $model) {
                        $t = 'update/'.$model->id;
                        return Html::a('', Url::to($t), ['class' => 'glyphicon glyphicon-pencil btn btn-default custom_button']);
                    },
                ],
            ],
        ],
    ]); ?>
    <p>
        <?= Html::a('Markt anlegen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
