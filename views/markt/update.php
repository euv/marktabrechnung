<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Markt */

$this->title = 'Markt bearbeiten:';
$this->params['breadcrumbs'][] = ['label' => 'Märkte', 'url' => ['admin']];
$this->params['breadcrumbs'][] = 'bearbeiten';
?>
<div class="markt-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
