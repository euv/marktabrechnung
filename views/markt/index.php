<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Markt';
$this->params['breadcrumbs'][] = $this->title;
?>

<script>
    var setMarketId = function(obj){
        var setMarkup = function(obj){
            var unsetAllSelected = function(){
                var buttonObjs = document.getElementsByClassName('markt');
                for (let i = 0; i < buttonObjs.length; i++){
                    buttonObjs[i].classList.remove("btn-dark");
                }
            }
            unsetAllSelected();
            obj.classList.add("btn-dark");
        }
        var invertDays = function(markttage){
            let marttageInv = [];
            for (let i = 0; i < 7; i++){
                if (markttage.indexOf(i) === -1){
                    marttageInv.push(i);
                }
            }
            return marttageInv.join();
        }

        setMarkup(obj);

        var id = $(obj).attr("data-marktid");
        var markttage = $(obj).attr("data-markttage").replace('7', '0');
        markttage = invertDays(markttage);

        $('#Markttage').removeAttr("disabled").parent().datepicker("setDaysOfWeekDisabled", markttage);
        $('#todaybtn').removeAttr("disabled");
        $('#nextbutton').attr("data-marktid", id);
    }

    var setDateInit = function(){
        $('#Markttage').attr('Disabled','disabled');
        $('#todaybtn').attr('Disabled','disabled');
    }

    var setDate = function(date){
        $('#nextbutton').attr("data-date", date);
        if($('#nextbutton').attr("data-marktid") && $('#nextbutton').attr("data-date")){
            $('#nextbutton').removeAttr("disabled");
        }
    }

    var setTodayDate = function(){
        const today = new Date();
        function formatDate(date, format) {
            const map = {
                mm: date.getMonth() + 1,
                dd: date.getDate(),
                yy: date.getFullYear().toString().slice(-2),
                yyyy: date.getFullYear()
            }

            return format.replace(/mm|dd|yyyy|yy/gi, matched => map[matched])
        }
        let todayDate = formatDate(today, 'dd.mm.yyyy');
        $('#Markttage').parent().datepicker("update", todayDate);
        setDate(todayDate);
    }

    var setNextUrl = function(obj){
        var id = obj.getAttribute("data-marktid");
        var date = obj.getAttribute("data-date");
        if (id != '' && date != ''){
            let dateArr = date.split(".");
            date = dateArr[2]+'-'+dateArr[1]+'-'+dateArr[0];
            document.location.href = 'buchung/index-filtered?marktid=' + id + '&date=' + date;
        } else {
            alert('Bitte Markt und Datum auswählen.');
        }
    }
</script>

<div class="markt-index">
    <div class="row">
            <div class="col-md-4">
                <h2><?= Html::encode('Marktauswahl') ?></h2>
                <div id="marketlist">
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'summary' => '',
                        'itemOptions' => ['class' => 'item'],
                        'itemView' => function ($model, $key, $index, $widget) {
                            return '<p class="btn-toolbar">'.Html::a(
                                    Html::encode($model->getButtonLabel()),
                                    'javascript:void(0)',
                                    [
                                        'class' => $model->getButtonStyle(),
                                        'onclick' => 'setMarketId(this)',
                                        'data' => ['marktid' => $model->MarktID, 'markttage' => $model->Markttage]
                                    ]).$model->infoButton.'</p>';
                        },
                    ]) ?>
                </div>
            </div>
            <div class="col-md-4">
                <h2><?= Html::encode('Datumsauswahl') ?></h2>
                <p> <?= Html::a('Heute', 'javascript:void(0)', [
                        'id' => 'todaybtn',
                        'class' => 'todaybtn btn btn-primary',
                        'onclick' => 'setTodayDate()',
                    ]) ?></p>
                <p>
                <?= DatePicker::widget([
                    'id' => 'Markttage',
                    'name' => 'Markttage',
                    'value' => date('d.m.Y'),
                    'template' => '{addon}{input}',
                    'language'=>'de',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                        'todayHighlight' => true,
                    ],
                    'clientEvents' => [
                        'ready' => 'setDateInit()',
                        'change' => 'function (e) {
                            var pickeddate = e.target.value;
                            setDate(pickeddate);
                        }',
                    ],
                ]);?></p>
            </div>
        </div>
    <hr>

    <div class="col">
        <div class="form-group">
            <?= Html::a(
                    'Weiter',
                    'javascript:void(0)',
                    [
                        'id' => 'nextbutton',
                        'class' => 'btn btn-primary pull-right',
                        'disabled' => 'disabled',
                        'data' => ['marktid' => '', 'date' => ''],
                        'onClick' => 'setNextUrl(this)'
                    ])
            ?>
        </div>
    </div>
</div>
