<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Markt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="markt-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Bezeichnung')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MarktID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MarkttageCheckboxes')->checkboxList($model->getMarkttageLabels()) ?>

    <?= $form->field($model, 'Kostenstelle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kostenträger')->textInput(['maxlength' => true]) ?>

    <?php if(Yii::$app->controller->action->id == 'update'): ?>
        <?= $form->field($model, 'status')->dropDownList(['0' => 'deaktiviert', '1' => 'aktiviert']) ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Speichern', ['class' => 'btn btn-success']) ?>
        <?php
        if(Yii::$app->controller->action->id == 'update'){
            echo Html::a('Löschen', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Sind Sie sicher?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
