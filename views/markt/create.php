<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Markt */

$this->title = 'Markt anlegen';
$this->params['breadcrumbs'][] = ['label' => 'Märkte', 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="markt-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
