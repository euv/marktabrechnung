<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Markt */

$this->title = $model->Bezeichnung;
$this->params['breadcrumbs'][] = ['label' => 'Märkte', 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="markt-view">

    <h1><?= Html::encode($this->title) ?></h1>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Bezeichnung',
            'MarktID',
            [
                'label' => 'Markttage',
                'value' => $model->_Markttage,
            ],
            'Kostenstelle',
            'Kostenträger',
            [
                'label' => 'Status',
                'value' => $model->_Status,
            ],
        ],
    ]) ?>

    <p>
        <?= Html::a('Bearbeiten', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

</div>
