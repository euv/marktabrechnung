<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BuchungSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Buchungen';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="buchung-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'debitorIDAndName',
            [
                'attribute' => 'marktIDAndName',
                'filter'    => $searchModel->MarktList
            ],
            'Datum',
            'Meter',
            'StromVerbrauch',
            [
                'attribute' => '_status',
                'filter'    => $searchModel->StatusList
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons'=>[
                    'view'=>function ($url, $model) {
                        $t = '/buchung/view/'.$model->id;
                        return Html::a('', Url::to($t), ['class' => 'glyphicon glyphicon-eye-open btn btn-default custom_button']);
                    },
                ],
            ],
        ],
    ]);
    ?>
    <p>
        <?= Html::a('Buchung erstellen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

</div>
