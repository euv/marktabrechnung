<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Buchung */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$kundeVal = [$kunde->id => $kunde->KundenID . '-' . $kunde->Vorname. ' ' . $kunde->Nachname];
$marktVal = [$markt->id => $markt->MarktID . '-' . $markt->Bezeichnung];

?>

<div class="buchung-form container">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'Debitor', ['options' => ['class' => 'col col-sm-3']])->dropDownList($kundeVal, ['readonly' => true])  ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'MarktId', ['options' => ['class' => 'col col-sm-3']])->dropDownList($marktVal, ['readonly' => true])  ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'Datum', ['options' => ['class' => 'col col-sm-3']])->textInput(['class' => 'form-control', 'readonly' => true]) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'Meter', ['options' => ['class' => 'col col-sm-3']])->textInput(['class' => 'form-control']) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'StromVerbrauch', ['options' => ['class' => 'col col-sm-3']])->textInput(['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Erfassen', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
