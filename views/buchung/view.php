<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Buchung */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Buchungen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="buchung-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'KundenID',
                'value' => $model->debitor->NameWithKdNr,
            ],
            [
                'label' => 'MarktId',
                'value' => $model->markt->NameWithId,
            ],
            [
                'label' => 'Datum',
                'value' => $model->GermanDate,
            ],
            'Meter',
            'StromVerbrauch',
            [
                'label' => 'Status',
                'value' => $model->lockstatus,
            ],
        ],
    ]) ?>

    <p>
        <?= Html::a('Stornieren', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Speichern als Storno?',
                'method' => 'post',
            ],
        ]) ?>

    </p>


</div>
