<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Buchung */

$this->title = 'Buchung bearbeiten:';
$this->params['breadcrumbs'][] = ['label' => 'Buchungen', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Bearbeiten';
?>
<div class="buchung-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
