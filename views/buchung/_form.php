<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Buchung */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buchung-form container">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'Debitor', ['options' => ['class' => 'col col-sm-3']])->dropDownList($model->Debitorlist)  ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'MarktId', ['options' => ['class' => 'col col-sm-3']])->dropDownList($model->Marketlist)  ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'Datum', ['options' => ['class' => 'col col-sm-3']])->widget(\yii\jui\DatePicker::class, [
            'options' => ['class' => 'form-control'],
        ]) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'Meter', ['options' => ['class' => 'col col-sm-3']])->textInput(['class' => 'form-control']) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'StromVerbrauch', ['options' => ['class' => 'col col-sm-3']])->textInput(['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?php if (Yii::$app->controller->action->id == 'create'): ?>
            <?= Html::submitButton('Erfassen', ['class' => 'btn btn-success']) ?>
        <?php else: ?>
            <?= Html::submitButton('Speichern', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Stornieren', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Sind Sie sicher?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
