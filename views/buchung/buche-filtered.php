<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Buchung */
$this->title = 'Buchung erstellen';
$this->params['breadcrumbs'][] = ['label' => 'Markt', 'url' => ['/markt']];
$this->params['breadcrumbs'][] = ['label' => 'Buchung', 'url' => ['index-filtered','marktid'=>$params['marktid'],'date'=>$params['date']]];
$this->params['breadcrumbs'][] = ['label' => 'Kunden', 'url' => ['create-filtered','marktid'=>$params['marktid'],'date'=>$params['date']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buchung-create">

    <?= $this->render('_form-filtered', [
        'model' => $model,
        'markt' => $markt,
        'kunde' => $kunde,
        'params' => $params,
    ]) ?>

</div>
