<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Buchung */

$this->title = 'Kunde erstellen';
$this->params['breadcrumbs'][] = ['label' => 'Markt', 'url' => ['/markt']];
$this->params['breadcrumbs'][] = ['label' => 'Buchung', 'url' => ['index-filtered','marktid'=>$params['marktid'],'date'=>$params['date']]];
$this->params['breadcrumbs'][] = ['label' => 'Kunden', 'url' => ['create-filtered','marktid'=>$params['marktid'],'date'=>$params['date']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kunde-create">

    <?= $this->render('_form-kunde-filtered', [
        'model' => $model,
    ]) ?>

</div>
