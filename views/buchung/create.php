<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Buchung */

$this->title = 'Buchung erstellen';
$this->params['breadcrumbs'][] = ['label' => 'Buchungen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buchung-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
