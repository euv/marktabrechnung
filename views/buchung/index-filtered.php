<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BuchungSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Buchungen';
$this->params['breadcrumbs'][] = ['label' => 'Markt', 'url' => ['/markt']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buchung-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'debitorIDAndName',
            'marktIDAndName',
            'Datum',
            'Meter',
            'StromVerbrauch',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons'=>[
                    'view'=>function ($url, $model) {
                        $t = 'view-filtered?id='.$model->id.'&marktid='.$_GET['marktid'].'&date='.$_GET['date'];
                        return Html::a('', Url::to($t), ['class' => 'glyphicon glyphicon-eye-open btn btn-default custom_button']);
                    },
                ],
            ],
        ],
    ]); ?>
    <p>
        <?php $url = Url::to(['create-filtered', 'marktid' => $params['marktid'], 'date' => $params['date']]);?>
        <?= Html::a('Buchung erstellen', $url, ['class' => 'btn btn-success']) ?>
    </p>

</div>
