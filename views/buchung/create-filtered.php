<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KundeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kunden';
$this->params['breadcrumbs'][] = ['label' => 'Markt', 'url' => ['/markt']];
$this->params['breadcrumbs'][] = ['label' => 'Buchung', 'url' => ['index-filtered','marktid'=>$params['marktid'],'date'=>$params['date']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kunde-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'KundenID',
            '_NameVorname',
            '_StrasseNr',
            'Ort',
            'GewArt:ntext',
            '_KundenArt',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {ok}',
                'contentOptions' => ['class' => 'nobr'],
                'buttons'=>[
                    'update'=>function ($url, $model) {
                        $t = 'update-kunde-filtered?marktid='.$_GET['marktid'].'&date='.$_GET['date'].'&kunde='.$model->KundenID;
                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-pencil btn btn-default"]);
                        return Html::a($icon, Url::to($t));
                    },
                    'ok'=>function ($url, $model) {
                        $t = 'buche-filtered?marktid='.$_GET['marktid'].'&date='.$_GET['date'].'&kunde='.$model->KundenID;
                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-arrow-right btn btn-default"]);
                        return Html::a($icon, Url::to($t));
                    },
                ],
            ],
        ],
    ]); ?>
    <p>
        <?= Html::a('Kunde anlegen', ['create-kunde-filtered', 'marktid'=>$params['marktid'], 'date'=>$params['date']], ['class' => 'btn btn-success']) ?>
    </p>

</div>
