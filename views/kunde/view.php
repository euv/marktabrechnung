<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kunde */

$this->title = $model->Vorname.' '.$model->Nachname;
$this->params['breadcrumbs'][] = ['label' => 'Kunden', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="kunde-view">

<!--    <h1><?/*= Html::encode($this->title) */?></h1>-->


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'KundenID',
            [
                'label' => 'Name',
                'value' => $model->Vorname .' '.$model->Nachname,
            ],
            [
                'format' => 'html',
                'label' => 'Anschrift',
                'value' => $model->Straße .' '.$model->Hausnummer.'<br>'.$model->PLZ .' '.$model->Ort.'<br>'.$model->Land,
            ],
            'GewArt:ntext',
            [
                'label' => 'KundenArt',
                'value' => ($model->KundenArt==0 ? 'Tag' : 'Dauer'),
            ],
            'DebitorNr',
            [
                'label' => 'Status',
                'value' => $model->lockstatus,
            ],
        ],
    ]) ?>

    <p>
        <?= Html::a('Bearbeiten', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>


</div>
