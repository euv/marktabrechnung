<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kunde */

$this->title = 'Kunde anlegen';
$this->params['breadcrumbs'][] = ['label' => 'Kunden', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kunde-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
