<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KundeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kunden';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kunde-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'KundenID',
            '_NameVorname',
            '_StrasseNr',
            'Ort',
            'GewArt:ntext',
            [
                'attribute' => '_KundenArt',
                'filter'    => $searchModel->KundenArtList
            ],
            'DebitorNr',
            [
                'attribute' => '_status',
                'filter'    => $searchModel->StatusList
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons'=>[
                    'update'=>function ($url, $model) {
                        $t = '/kunde/update/'.$model->id;
                        return Html::a('', Url::to($t), ['class' => 'glyphicon glyphicon-pencil btn btn-default custom_button']);
                    },
                ]
            ],
        ],
    ]); ?>
    <p>
        <?= Html::a('Kunde anlegen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

</div>
