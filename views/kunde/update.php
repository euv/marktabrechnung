<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kunde */

$this->title = 'Kunde bearbeiten';
$this->params['breadcrumbs'][] = ['label' => 'Kunden', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Bearbeiten';
?>
<div class="kunde-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
