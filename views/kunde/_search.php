<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KundeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kunde-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'KundenID') ?>

    <?= $form->field($model, 'Vorname') ?>

    <?= $form->field($model, 'Nachname') ?>

    <?= $form->field($model, 'Straße') ?>

    <?php // echo $form->field($model, 'Hausnummer') ?>

    <?php // echo $form->field($model, 'PLZ') ?>

    <?php // echo $form->field($model, 'Ort') ?>

    <?php // echo $form->field($model, 'Land') ?>

    <?php // echo $form->field($model, 'GewArt') ?>

    <?php // echo $form->field($model, 'KundenArt') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'modified_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
