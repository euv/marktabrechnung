<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kunde */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kunde-form container">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'KundenID', ['options' => ['class' => 'col col-sm-3']])->textInput(['class' => 'form-control', 'disabled' => !$model->isNewRecord]) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'Vorname', ['options' => ['class' => 'col col-sm-3']])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
        <?= $form->field($model, 'Nachname', ['options' => ['class' => 'col col-sm-3']])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'Straße', ['options' => ['class' => 'col col-sm-5']])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
        <?= $form->field($model, 'Hausnummer', ['options' => ['class' => 'col col-sm-1']])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'PLZ', ['options' => ['class' => 'col-sm-2']])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
        <?= $form->field($model, 'Ort', ['options' => ['class' => 'col-sm-4']])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'Land', ['options' => ['class' => 'col-sm-6']])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'GewArt', ['options' => ['class' => 'col-sm-6']])->textarea(['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Bsp.: Obst, Fleisch, Gemüse, Backwaren']) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'KundenArt', ['options' => ['class' => 'col-sm-6']])->dropDownList(['0' => 'Tag', '1' => 'Dauer', '2' => 'Propagandist']) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'DebitorNr', ['options' => ['class' => 'col-sm-6']])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    </div>
    <?php if(Yii::$app->controller->action->id == 'update'): ?>
    <div class="row">
        <?= $form->field($model, 'status', ['options' => ['class' => 'col-sm-6']])->dropDownList(['0' => 'gesperrt', '1' => 'entsperrt']) ?>
    </div>
    <?php endif; ?>
    <div class="form-group">
        <?= Html::submitButton('Übernehmen', ['class' => 'btn btn-success']) ?>
    <?php
    if(Yii::$app->controller->action->id == 'update'){
        echo Html::a('Löschen', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Sind Sie sicher?',
                'method' => 'post',
            ],
        ]);
    }
    ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
