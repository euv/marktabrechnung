<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/images/EUV_Logo_transp.png', ['alt'=>Yii::$app->name, 'class' => 'logo']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            Yii::$app->user->can("fibu-management") ? (
            ['label' => 'Listen',
                'url' => ['#'],
                'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                    ['label' => 'Märkte', 'url' => '/markt/admin'],
                    ['label' => 'Kunden', 'url' => '/kunde'],
                    ['label' => 'Buchungen', 'url' => '/buchung'],
                    ['label' => 'Kat. Zuordnung', 'url' => '/kat-zuordnung'],
                    ['label' => 'Export', 'url' => '/export'],
                ],
            ]
            ) : (
            ''
            ),
            Yii::$app->user->can("user-management") ? (
                [
                    'label' => 'Admin',
                    'url' => ['#'],
                    'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => 'Benutzer/Rechte', 'url' => '/user/admin'],
                        ['label' => 'Marktinfos', 'url' => '/marktinfo'],
                    ],
                ]
            ) : (
                ''
            ),
            Yii::$app->user->isGuest ? (
                ''
            ) : (
                ['label' => 'Profil', 'url' => ['/user/settings/account']]
            ),
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/markt']]
            ) : (
                '<li>'
                . Html::beginForm(['/user/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"><?= Html::a('Gebührensatzung', ['/site/gebuehrensatzung']) ?></p>
        <p class="pull-left"><?= Html::a('Durchführung', ['/site/durchfuehrung']) ?></p>
        <p class="pull-left"><?= Html::a('Marktsatzung', ['/site/marktsatzung']) ?></p>
        <p class="pull-left"><?= Html::a('Wochenmarktverordnung', ['/site/wochenmarktverordnung']) ?></p>
        <p class="pull-right">&copy; EUV <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
