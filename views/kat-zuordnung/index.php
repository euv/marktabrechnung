<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KatZuordnungSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategorie-Zuordnung';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kat-zuordnung-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => '_KundenArt',
                'filter'    => $searchModel->KundenArtListe
            ],
            [
                'attribute' => '_MarktIDAndName',
                'filter'    => $searchModel->Marketlist
            ],
            [
                'attribute' => '_Verbrauch',
                'filter'    => $searchModel->VerbrauchListe
            ],
            [
                'attribute' => 'Kategorie',
                'filter'    => $searchModel->KategorieList
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons'=>[
                    'update'=>function ($url, $model) {
                        $t = '/kat-zuordnung/update/'.$model->id;
                        return Html::a('', Url::to($t), ['class' => 'glyphicon glyphicon-pencil btn btn-default custom_button']);
                    },
                ]
            ],
        ],
    ]); ?>
    <p>
        <?= Html::a('Kategorie-Zuordnung anlegen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

</div>
