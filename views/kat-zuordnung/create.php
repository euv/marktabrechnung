<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KatZuordnung */

$this->title = 'Kategorie-Zuordnung anlegen';
$this->params['breadcrumbs'][] = ['label' => 'Kategorie-Zuordnung', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Anlegen';
?>
<div class="kat-zuordnung-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
