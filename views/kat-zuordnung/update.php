<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KatZuordnung */

$this->title = 'Kategorie-Zuordnung bearbeiten';
$this->params['breadcrumbs'][] = ['label' => 'Kategorie-Zuordnung', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Bearbeiten';
?>
<div class="kat-zuordnung-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
