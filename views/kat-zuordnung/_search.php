<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KatZuordnungSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kat-zuordnung-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'KundenArt') ?>

    <?= $form->field($model, 'Markt') ?>

    <?= $form->field($model, 'Verbrauch') ?>

    <?= $form->field($model, 'Kategorie') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
