<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KatZuordnung */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kat-zuordnung-form container">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'Markt', ['options' => ['class' => 'col-sm-12']])->dropDownList($model->MarketList) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'KundenArt', ['options' => ['class' => 'col-sm-4']])->dropDownList($model->KundenArtListe) ?>
        <?= $form->field($model, 'Verbrauch', ['options' => ['class' => 'col-sm-4']])->dropDownList($model->VerbrauchListe) ?>
        <?= $form->field($model, 'Kategorie', ['options' => ['class' => 'col-sm-4']])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Übernehmen', ['class' => 'btn btn-success']) ?>
        <?php
        if(Yii::$app->controller->action->id == 'update') {
            echo Html::a('Löschen', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Sind Sie sicher?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
