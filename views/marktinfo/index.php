<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MarktinfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Marktinfos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marktinfo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            '_markt_id',
            'infosite',
            'params:ntext',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons'=>[
                    'update'=>function ($url, $model) {
                        $t = '/marktinfo/update/'.$model->id;
                        return Html::a('', Url::to($t), ['class' => 'glyphicon glyphicon-pencil btn btn-default custom_button']);
                    },
                ]
            ],
        ],
    ]); ?>
    <p>
        <?= Html::a('Marktinfo anlegen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

</div>
