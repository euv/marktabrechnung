<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Marktinfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marktinfo-form container">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'markt_id', ['options' => ['class' => 'col col-3']])->dropDownList($model->Marketlist)  ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'infosite')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'params')->textarea(['rows' => 6]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Speichern', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
