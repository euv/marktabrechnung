<?php
use yii\helpers\Html;

$this->title = 'Marktinfos';
$this->params['breadcrumbs'][] = ['label' => 'Markt', 'url' => ['/markt']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-4">
        <p><strong>Habinghorst</strong></p>
        <p>Markttage: Mittwoch &amp; Samstag / von 8.oo bis 13.oo Uhr <br>
        <em>MarktID: 03, Kostenstelle: 345, Kostenträger: 678</em></p>
        <p>Viele Bürger freuen sich über das Angebot an Obst und
        Gemüse, Fleisch und Wurst sowie Textilien. <br>
        Diese traditionellere Variante des Einkaufens bietet, dank fachgerechter
        Beratung, oftmals einen regionalen Bezug zu den Produkten.</p>
    </div>
    <div class="col-md-8">
        <?php
        if (isset($params)){
            $p = json_decode($params, true);
            if (isset($p['imgPath'])) {
                echo Html::img('@web/images/maerkte/'.$p['imgPath'], ['class'=>'img-responsive']);
            }
        }
        ?>
    </div>
</div>


