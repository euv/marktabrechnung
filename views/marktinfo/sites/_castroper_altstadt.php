<?php
    use yii\helpers\Html;

    $this->title = 'Marktinfos';
    $this->params['breadcrumbs'][] = ['label' => 'Markt', 'url' => ['/markt']];
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-4">
        <p><strong>Castroper Altstadt</strong></p>
        <p>Markttage: Dienstag, Donnerstag &amp; Samstag / von 8.oo
        bis 13.oo Uhr <br>
        <em>MarktID: 01, Kostenstelle: 123, Kostenträger: 456</em></p>
        <p>Der größte Markt in Castrop-Rauxel. Hier bieten gleich vier
        bis fünf Anbieter frisches Obst und Gemüse. <br>
        Mindestens drei Fleisch- und Wurststände verkaufen ihre Produkte. <br>
        Auch Geflügel, Fisch, Käse und Kartoffeln werden meist an mehreren Ständen
        angeboten. <br>
        Backwaren, Eier, Tee und Gewürze ergänzen die Produktpalette auf dem
        Altstadtmarkt. <br>
        Aber auch Textilien, Lederwaren und Blumen kann man hier kaufen. <br>
        Donnerstags sind auch Kurzwaren, Miederwaren, Unterwäsche, Tischdecken,
        Gardinen, Deko und Uhren zu bekommen.</p>
        <p>Kurze Wege und eine sehr gute Anbindung bietet neben dem
        Wochenmarkt die Castroper Innenstadt. Viele kleine Fachgeschäfte laden zum
        Bummeln und Einkaufen ein. <br>
        Auch der öffentliche Nahverkehr sorgt mit Bussen für eine gute Anbindung ins
        Zentrum. <br>
        Regelmäßige Veranstaltungen und Aktionen runden das vielseitige Angebot in der
        Altstadt ab und sind immer einen Besuch wert.
        </p>
    </div>
    <div class="col-md-8">
        <?php
            if (isset($params)){
                $p = json_decode($params, true);
                if (isset($p['imgPath'])) {
                    echo Html::img('@web/images/maerkte/'.$p['imgPath'], ['class'=>'img-responsive']);
                }
            }
        ?>
    </div>
</div>
