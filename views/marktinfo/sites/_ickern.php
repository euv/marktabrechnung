<?php
use yii\helpers\Html;

$this->title = 'Marktinfos';
$this->params['breadcrumbs'][] = ['label' => 'Markt', 'url' => ['/markt']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-4">
        <p><strong>Ickern</strong></p>
        <p>Markttage: Dienstag &amp; Freitag / von 8.oo bis 13.oo Uhr <br>
        <em>MarktID: 02, Kostenstelle: 234, Kostenträger: 567</em></p>
        <p>Obst und Gemüse werden in Ickern gleich an drei Ständen
        angeboten. <br>
        Auch bei Fleisch, Wurst und Geflügel sowie Fisch hat man die Wahl bei mehreren
        Händlern. <br>
        Textilien, Blumen und Pflanzen sowie Bettwäsche gibt es an beiden Markttagen;
        Backwaren, Kartoffeln, Kurzwaren, Miederwaren und Tischdecken nur freitags. <br>
        Freitags ergänzt zudem ein Uhrenanbieter das Sortiment.</p>
    </div>
    <div class="col-md-8">
        <?php
        if (isset($params)){
            $p = json_decode($params, true);
            if (isset($p['imgPath'])) {
                echo Html::img('@web/images/maerkte/'.$p['imgPath'], ['class'=>'img-responsive']);
            }
        }
        ?>
    </div>
</div>

