<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Marktinfo */

$this->title = $model->infosite;
$this->params['breadcrumbs'][] = ['label' => 'Marktinfos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="marktinfo-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'markt_id',
            'infosite',
            'params:ntext',
        ],
    ]) ?>

    <p>
        <?= Html::a('Bearbeiten', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Löschen', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Sind Sie sicher?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
