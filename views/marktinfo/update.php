<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Marktinfo */

$this->title = 'Marktinfo bearbeiten';
$this->params['breadcrumbs'][] = ['label' => 'Marktinfos', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Bearbeiten';
?>
<div class="marktinfo-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
