<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Marktinfo */

$this->title = 'Marktinfo anlegen';
$this->params['breadcrumbs'][] = ['label' => 'Marktinfos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marktinfo-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
