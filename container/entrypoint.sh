#!/bin/bash

DB_CONFIG=/app/config/db.php

cat > $DB_CONFIG <<+END
<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => getenv("DB_DSN"),
    'username' => getenv("DB_USERNAME"),
    'password' => getenv("DB_PASSWORD"),
    'charset' => 'utf8',
    "attributes" => [
        PDO::MYSQL_ATTR_SSL_CA => getenv("DB_CA_CERT"),
    ],

    // Schema cache options (for production environment)
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
+END

echo "$@"

docker-php-entrypoint "$@"
