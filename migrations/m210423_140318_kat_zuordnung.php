<?php

use yii\db\Migration;

/**
 * Class m210423_140318_kat_zuordnung
 */
class m210423_140318_kat_zuordnung extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `markt`.`kat_zuordnung` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `KundenArt` INT(11) NOT NULL,
          `Markt` INT(11) NOT NULL,
          `Verbrauch` INT(2) NOT NULL,
          `Kategorie` VARCHAR(45) NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `fk_kat_zuordnung_markt1_idx` (`Markt` ASC),
          CONSTRAINT `fk_kat_zuordnung_markt1`
            FOREIGN KEY (`Markt`)
            REFERENCES `markt`.`markt` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_unicode_ci");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("kat_zuordnung");
    }
}
