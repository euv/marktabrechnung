<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%buchung}}`.
 */
class m210409_092033_add_isFirstBooking_column_to_buchung_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%buchung}}', 'isFirstBooking', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%buchung}}', 'isFirstBooking');
    }
}
