SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `auth_assignment` (
                                   `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
                                   `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
                                   `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('administrator', '1', 1607476440),
('editor', '2', 1608123612),
('fibu', '3', 1608232413);

CREATE TABLE `auth_item` (
                             `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
                             `type` smallint(6) NOT NULL,
                             `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
                             `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
                             `data` blob DEFAULT NULL,
                             `created_at` int(11) DEFAULT NULL,
                             `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrator', 1, 'Administrator', NULL, NULL, 1607476440, 1608123924),
('editor', 1, 'darf Marktstände administrieren', NULL, NULL, 1608122938, 1608649365),
('fibu', 1, 'darf FiBu administrieren', NULL, NULL, 1608123696, 1608649345),
('fibu-management', 2, 'darf FiBu verwalten', NULL, NULL, 1608123052, 1608123052),
('market-management', 2, 'darf Kunden verwalten', NULL, NULL, 1608123019, 1608130093),
('user-management', 2, 'User Management', NULL, NULL, 1607476440, 1607476440);

CREATE TABLE `auth_item_child` (
                                   `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
                                   `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('administrator', 'editor'),
('administrator', 'fibu'),
('administrator', 'user-management'),
('editor', 'market-management'),
('fibu', 'editor'),
('fibu', 'fibu-management'),
('fibu', 'market-management');

CREATE TABLE `auth_rule` (
                             `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
                             `data` blob DEFAULT NULL,
                             `created_at` int(11) DEFAULT NULL,
                             `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `buchung` (
                           `id` int(11) NOT NULL,
                           `Debitor` int(11) NOT NULL,
                           `MarktId` int(11) NOT NULL,
                           `Datum` date NOT NULL,
                           `Meter` float DEFAULT NULL,
                           `StromVerbrauch` float DEFAULT NULL,
                           `created_at` timestamp NULL DEFAULT current_timestamp(),
                           `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                           `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `kunde` (
                         `id` int(11) NOT NULL,
                         `KundenID` int(11) NOT NULL,
                         `Vorname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `Nachname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `Straße` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `Hausnummer` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
                         `PLZ` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
                         `Ort` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `Land` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `GewArt` text COLLATE utf8_unicode_ci DEFAULT NULL,
                         `DebitorNr` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
                         `KundenArt` int(11) NOT NULL,
                         `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
                         `modified_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                         `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `markt` (
                         `id` int(11) NOT NULL,
                         `Bezeichnung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `MarktID` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
                         `Markttage` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
                         `Kostenstelle` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
                         `Kostenträger` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
                         `created_at` timestamp NULL DEFAULT current_timestamp(),
                         `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                         `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `markt` (`id`, `Bezeichnung`, `MarktID`, `Markttage`, `Kostenstelle`, `Kostenträger`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Castroper Altstadt', '01', '2,4,6', '123', '456', '2020-12-16 13:22:25', '2020-12-22 18:54:36', 1),
(2, 'Ickern', '02', '2,5', '234', '567', '2020-12-22 09:24:26', '2020-12-22 19:58:47', 1),
(3, 'Habinghorst', '03', '3,6', '345', '678', '2020-12-22 09:24:59', '2020-12-22 19:59:50', 1);

CREATE TABLE `marktinfo` (
                             `id` int(11) NOT NULL,
                             `markt_id` int(11) NOT NULL,
                             `infosite` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                             `params` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `marktinfo` (`id`, `markt_id`, `infosite`, `params`) VALUES
(1, 1, '_castroper_altstadt', '{\r\n   \"imgPath\":\"Festsetzungsflaeche_Wochenmarkt_Altstadt.jpg\"\r\n}'),
(2, 2, '_ickern', '{\r\n   \"imgPath\":\"Festsetzungsflaeche_Marktplatz_Ickern.jpg\"\r\n}'),
(3, 3, '_habinghorst', '{\r\n   \"imgPath\":\"Festsetzungsflaeche_Marktplatz_Habinghorst.jpg\"\r\n}');

CREATE TABLE `migration` (
                             `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
                             `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('Da\\User\\Migration\\m000000_000001_create_user_table', 1607474927),
('Da\\User\\Migration\\m000000_000002_create_profile_table', 1607474927),
('Da\\User\\Migration\\m000000_000003_create_social_account_table', 1607474927),
('Da\\User\\Migration\\m000000_000004_create_token_table', 1607474927),
('Da\\User\\Migration\\m000000_000005_add_last_login_at', 1607474927),
('Da\\User\\Migration\\m000000_000006_add_two_factor_fields', 1607474927),
('Da\\User\\Migration\\m000000_000007_enable_password_expiration', 1607474927),
('Da\\User\\Migration\\m000000_000008_add_last_login_ip', 1607474927),
('Da\\User\\Migration\\m000000_000009_add_gdpr_consent_fields', 1607474928),
('m000000_000000_base', 1607375171),
('m140506_102106_rbac_init', 1607475373),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1607475373),
('m180523_151638_rbac_updates_indexes_without_prefix', 1607475373),
('m200409_110543_rbac_update_mssql_trigger', 1607475373),
('m201209_010702_create_admin_user', 1607476440);

CREATE TABLE `profile` (
                           `user_id` int(11) NOT NULL,
                           `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                           `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                           `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                           `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
                           `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                           `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                           `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
                           `bio` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `timezone`, `bio`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

CREATE TABLE `token` (
                         `user_id` int(11) DEFAULT NULL,
                         `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
                         `type` smallint(6) NOT NULL,
                         `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `user` (
                        `id` int(11) NOT NULL,
                        `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                        `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                        `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
                        `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
                        `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                        `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
                        `flags` int(11) NOT NULL DEFAULT 0,
                        `confirmed_at` int(11) DEFAULT NULL,
                        `blocked_at` int(11) DEFAULT NULL,
                        `updated_at` int(11) NOT NULL,
                        `created_at` int(11) NOT NULL,
                        `last_login_at` int(11) DEFAULT NULL,
                        `last_login_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
                        `auth_tf_key` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
                        `auth_tf_enabled` tinyint(1) DEFAULT 0,
                        `password_changed_at` int(11) DEFAULT NULL,
                        `gdpr_consent` tinyint(1) DEFAULT 0,
                        `gdpr_consent_date` int(11) DEFAULT NULL,
                        `gdpr_deleted` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `unconfirmed_email`, `registration_ip`, `flags`, `confirmed_at`, `blocked_at`, `updated_at`, `created_at`, `last_login_at`, `last_login_ip`, `auth_tf_key`, `auth_tf_enabled`, `password_changed_at`, `gdpr_consent`, `gdpr_consent_date`, `gdpr_deleted`) VALUES
(1, 'admin', 'admin@example.com', '$2y$10$m5Qi/o1a5G9/emNBiEP4F.SEdkYW2NaN0dQXdBO5ZGJZg2BYfFNX6', '8ddwjxm5fnVggVJy6pmNgbDH9m5PFhZ-', NULL, NULL, 0, 1607476440, NULL, 1608125475, 1607476440, 1608659981, '::1', '', 0, 1607476440, 0, NULL, 0),
(2, 'editor', 'editor@example.com', '$2y$10$88endm/Hs5vuYv/A2lEw5uR3BBO7OozJo1lNEHtXfvXGjee/qwdEi', 'wmwH8ZZuYOUrGOTJe2GS9-6G-pkqkT-e', NULL, '::1', 0, 1608123184, NULL, 1608123184, 1608123184, 1608659565, '::1', '', 0, 1608123184, 0, NULL, 0),
(3, 'fibuch', 'fibu@example.com', '$2y$10$eHmq3dR20yrGZBTba7Xrbu5dBcA437pFlDnaVrurEOf7b/AIhRjyu', 'NXpyWftHmg8uUU4GWGS1O5uPHpu_gAkD', NULL, '::1', 0, 1608123840, NULL, 1608232157, 1608123840, 1608659589, '::1', '', 0, 1608232157, 0, NULL, 0);


ALTER TABLE `auth_assignment`
    ADD PRIMARY KEY (`item_name`,`user_id`),
    ADD KEY `idx-auth_assignment-user_id` (`user_id`);

ALTER TABLE `auth_item`
    ADD PRIMARY KEY (`name`),
    ADD KEY `rule_name` (`rule_name`),
    ADD KEY `idx-auth_item-type` (`type`);

ALTER TABLE `auth_item_child`
    ADD PRIMARY KEY (`parent`,`child`),
    ADD KEY `child` (`child`);

ALTER TABLE `auth_rule`
    ADD PRIMARY KEY (`name`);

ALTER TABLE `buchung`
    ADD PRIMARY KEY (`id`),
    ADD KEY `fk_Buchung_Kunde1_idx` (`Debitor`),
    ADD KEY `fk_Buchung_Märkte1_idx` (`MarktId`);

ALTER TABLE `kunde`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `markt`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `Bezeichnung_UNIQUE` (`Bezeichnung`),
    ADD UNIQUE KEY `MarktID_UNIQUE` (`MarktID`);

ALTER TABLE `marktinfo`
    ADD PRIMARY KEY (`id`),
    ADD KEY `fk_marktinfo_markt1_idx` (`markt_id`);

ALTER TABLE `migration`
    ADD PRIMARY KEY (`version`);

ALTER TABLE `profile`
    ADD PRIMARY KEY (`user_id`);

ALTER TABLE `token`
    ADD UNIQUE KEY `idx_token_user_id_code_type` (`user_id`,`code`,`type`);

ALTER TABLE `user`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `idx_user_username` (`username`),
    ADD UNIQUE KEY `idx_user_email` (`email`);


ALTER TABLE `buchung`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `kunde`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `markt`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `marktinfo`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `user`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;


ALTER TABLE `auth_assignment`
    ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `auth_item`
    ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `auth_item_child`
    ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `buchung`
    ADD CONSTRAINT `fk_Buchung_Kunde1` FOREIGN KEY (`Debitor`) REFERENCES `kunde` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_Buchung_Märkte1` FOREIGN KEY (`MarktId`) REFERENCES `markt` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `marktinfo`
    ADD CONSTRAINT `fk_marktinfo_markt1` FOREIGN KEY (`markt_id`) REFERENCES `markt` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `profile`
    ADD CONSTRAINT `fk_profile_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

ALTER TABLE `token`
    ADD CONSTRAINT `fk_token_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;
