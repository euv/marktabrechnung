FROM yiisoftware/yii2-php:7.4-apache

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get dist-upgrade -y && \
    #apt-get install -y 
    rm -rf /var/lib/apt/lists/*

VOLUME [ "/app/runtime" ]

COPY ./ /app
CMD [ "apache2-foreground" ]
ENTRYPOINT [ "/app/container/entrypoint.sh" ]
