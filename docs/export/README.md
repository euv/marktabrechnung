# Export für Infoma
Der Export der Daten für Infoma wird über zwei CSV Dateien realisiert.
Die Daten in Infoma werden über die "Zähler" Schnittstelle eingelesen.

Um die Daten in Infoma importieren zu können müssen in Infoma entsprechende
Debitoren angelegt sein. Die Debitoren Nr. muss dann im Makrtsystem bei
den Kunden hinterlegt werden (Debitor Nr).

## Export der Kategorien
Bei den Kategorien muss eine Export-Matrix angelegt werden. Diese
bezieht sich auf die Informationen aus den Kunden (KundenArt) und dem
Markt (MarktID).

Die Kategorien müssen pro Markt bzw. pro KundenArt definiert werden.

Die Matrix ist somit 3-Diminsional: KundenArt x Markt x Meter/kWh

Die Kategorien sollen frei konfiguriertbar sein.

### Beispiel Export Matrix für Meter
| | KundenArt Dauer | KunderArt Tag | KundenArt Propagandist |
|-|-|-|-|
| <b>Ickern</b> | 610 | 612 | 600 |
| <b>Habinghorst</b> | 620 | 622 | 600 |
| <b>Castrop</b> | 630 | 632 | 600

### Beispiel Export Martix für kWh
| | KundenArt Dauer | KundeArt Tag | KundeArt Propagandist |
|-|-|-|-|
| <b>Ickern</b> | 611 | 611 | 611 |
| <b>Habinghorst</b> | 611 | 611 | 611 |
| <b>Castrop</b> | 611 | 611 | 611 |

## Aufteilung der Arbeiten
Die Arbeiten werden in den folgenden Abteilungen wie folgt aufgeteilt:

### Erfassung der Daten vor Ort
Hier für sind die Marktmeister verantwortlich. Die Marktmeister erfassen 
pro Markt und pro Tag die Anwesenden Händler. Ist ein Händler im System noch
nicht hinterlegt, legt der Marktmeister diesen im System an. Danach wird
der Stand erfasst. Hier für wir die Meterzahl aufgenommen. Wenn der Händler
auch Strom benötigt, kann auch der Stromverbrauch erfasst werden. Die Erfassung
des Stromverbrauchs muss nicht und kann meist auch nicht, am selben Tag geschehen.
Der Verbrauchte Strom wird über die gleiche Buchungsmaske erfasst.
In der Buchungsmaske können sowohl Meter wie auch Stromverbrauch erfasst werden.
Eine der beiden Angaben (Meter / kWh) ist zwingend erforderlich, welche spielt
keine Rolle.

### Anlegen neuer Stammdaten
Falls der Marktmeister neue Stammdaten erfasst hat müssen diese im Infoma
eingepflegt werden. Für das Anlegen der Stammdaten (Debitor) ist die FiBu
verantwortlich. Die Information über den Händler werden von GBA bereitgestellt.

### Anlegen eines Marktes in Infoma
Falls ein Händler einen Stand das erste mal auf einen Markt betreibt, muss hierfür
auch ein Datensatz angeleft werden. Dieses soll über den Export geschehen.
Das Dateiformat hierfür ist eine CSV. Die Kopfzeile darf nicht mit exportiert werden und dient hier nur der Übersichtlichkeit.
- Pipe separiert
- ISO-8859-1
- Ende der Zeile \r\n
Die Felder sind die folgenden:

| Feld              | Datentyp | Beschreibung                                                                              |
|-------------------|----------|-------------------------------------------------------------------------------------------|
| Adressnummer      | String   | Die Debitorennummer aus den Stammdaten                                                    |
| Objektbezeichnung | String   | Wird zusammengesetzt aus Nachname, Vorname - Markt                                        |
| Zusatzinfomation  | String   | ID des Marktobjektes, wird zusammengesetzt aus: MarktID-Debitornnummer                    |
| Begindatum        | Date     | Erste des Monats für die erste Buchung des Händlers, wird aus den Buchungsdaten errechnet |

Somit sieht eine Beispieldatei wie folgt aus (Daten aus dem Testbereich):
```csv
Adressnummer|Objektbezeichnung|Zusatzinformatio|Begindatum
71111|Test 1 Nachname, Test 1 Vorname - Castroper Altstadt|1-71111|01.01.2021
71111|Test 1 Nachname, Test 1 Vorname - Habinghorst|2-71111|01.01.2021
72222|Test 2 Nachname, Test 2 Vorname - Habinghorst|2-72222|01.01.2021
72222|Test 2 Nachname, Test 2 Vorname - Ickern|3-72222|01.01.2021
71111|Test 1 Nachname, Test 1 Vorname - Ickern|3-71111|01.01.2021
```

### Anlegen der Buchungen in Infoma
Die Buchungsdaten für Infoma werden auch über einen Export erstellt. In der Maske für
das Erstellen der Exportdaten soll eine Voransicht der zu exportierenden Daten vorhanden sein.
Um den Export erstellen zu können ist ein Filter auf ein Datumszeitraum von einen Monat notwendig,
hierfür kann der Filter aus den anderen Masken übernommen werden.

Die Daten werden immer im Nachfolgemonat erstellt. Somit werden die Buchungsdaten für Januar,
frühstens am 1. Februar erstellt. Somit ist sichergestellt, das keine weiteren Buchungen mehr
hinzu kommen können, da diese ja ausschließlich vor Ort erfast werden.

Die Exportdatei muss auch wieder im CSV Format. Die Kopfzeile darf nicht mit exportiert werden und dient hier nur der Übersichtlichkeit.
Auch hier gilt wieder:
- Pipe separiert
- ISO-8859-1
- Ende der Zeile \r\n
Die Felder in der Datei sind folgende:

| Feld | Datentyp | Beschreibung |
|-|-|-|
| Zusatzinformation | String | Die ID des Marktobjektes in Informa, kann erstellt werden aus: MarktID-Debitorennummer |
| Menge | Integer | Die Menge beschreibt die Meteranzahl oder die kWh je nach Kategorie |
| Beginndatum | Date | Der Tag der Buchung |
| Endedatum | Date | Der Tag der Buchung +1 Tag |
| Kategorie | Integer | Die in Infoma angeleget ID der Kategorie. Diese wird im Marktsystem einmal in einer zentrallen Konfig festgelegt |

Für die Kategorien soll eine zentrale Konfiguration möglich sein. Siehe hierzu das Kapitel "Export der Kategorien".

Ein Beispiel für so eine Buchungsdatei wäre somit (die Daten beziehen sich auf das Testsystem):
```csv
Zusatzinfomation|Menge|Beginndatum|Endedatum|Kategorie
1-71111|2|14.1.2021|15.1.2021|510
1-71111|3|14.1.2021|15.1.2021|540
1-71111|3|15.1.2021|16.1.2021|510
2-71111|12|20.1.2021|21.1.2021|510
2-72222|33|20.1.2021|21.1.2021|510
2-72222|7|20.1.2021|21.1.2021|540
3-72222|44|20.1.2021|21.1.2021|510
3-71111|4|20.1.2021|21.1.2021|540
```
