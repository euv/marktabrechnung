<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kunde".
 *
 * @property int $id
 * @property int $KundenID
 * @property string $Vorname
 * @property string $Nachname
 * @property string $Straße
 * @property string $Hausnummer
 * @property string $PLZ
 * @property string $Ort
 * @property string $Land
 * @property string|null $GewArt
 * @property int $KundenArt
 * @property string $created_at
 * @property string|null $modified_at
 * @property int $status
 *
 * @property Buchung[] $buchungs
 */
class Kunde extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kunde';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KundenID', 'Vorname', 'Nachname', 'Straße', 'Hausnummer', 'PLZ', 'Ort', 'Land', 'KundenArt'], 'required'],
            [['KundenID', 'KundenArt', 'status'], 'integer'],
            [['GewArt','DebitorNr'], 'string'],
            [['KundenID'], 'unique'],
            [['created_at', 'modified_at'], 'safe'],
            [['Vorname', 'Nachname', 'Straße', 'Ort', 'Land'], 'string', 'max' => 255],
            [['Hausnummer', 'PLZ'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'KundenID' => 'KundenID',
            'Vorname' => 'Vorname',
            'Nachname' => 'Nachname',
            'Straße' => 'Straße',
            'Hausnummer' => 'Hausnr.',
            'PLZ' => 'PLZ',
            'Ort' => 'Ort',
            'Land' => 'Land',
            'GewArt' => 'Gewerbeart',
            'DebitorNr' => 'Debitor Nr.',
            'KundenArt' => 'Kundenart',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'status' => 'Sperrung',
            '_NameVorname' => 'Name, Vorname',
            '_StrasseNr' => 'Straße, Nr',
            '_KundenArt' => 'Kundenart',
        ];
    }

    /**
     * Gets query for [[Buchungs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBuchungs()
    {
        return $this->hasMany(Buchung::class, ['Debitor' => 'id']);
    }

    public function getDebitorNr()
    {
        return $this->DebitorNr;
    }

    public function getName()
    {
        return $this->Nachname . ' ' . $this->Vorname;
    }

    public function get_NameVorname()
    {
        return $this->name;
    }

    public function get_StrasseNr()
    {
        return $this->straßeNr;
    }

    public function get_KundenArt()
    {
        switch ($this->KundenArt) {
            case 0:
                $kundenArt = 'Tag';
                break;
            case 1:
                $kundenArt = 'Dauer';
                break;
            case 2:
                $kundenArt = 'Propagandist';
                break;
            default:
                $kundenArt = '';
        }
        return $kundenArt;
    }

    public function get_KundenArtReverse()
    {
        switch ($this->_KundenArt) {
            case 'Tag':
                $kundenArt = 0;
                break;
            case 'Dauer':
                $kundenArt = 1;
                break;
            case 'Propagandist':
                $kundenArt = 2;
                break;
            default:
                $kundenArt = null;
        }
        return $kundenArt;
    }

    public function getNameWithKdNr()
    {
        return $this->KundenID .' - '. $this->Vorname . ' ' . $this->Nachname;
    }

    public function getStraßeNr()
    {
        return $this->Straße . ' ' . $this->Hausnummer;
    }

    public function getLockstatus()
    {
        switch ($this->status) {
            case 0:
                return "gesperrt";
            case 1:
                return "entsperrt";
            case 2:
                return "gelöscht";
            default:
                return "";
        }
    }

    public function get_status()
    {
        return $this->lockstatus;
    }

    public function get_statusReverse()
    {
        switch ($this->_status) {
            case "gesperrt":
                return 0;
            case "entsperrt":
                return 1;
            case "gelöscht":
                return 2;
            default:
                return "";
        }
    }

    public function getLastId(){
        if( $lastidObj = $this->find()->orderBy(['id' => SORT_DESC])->one() ){
            return $lastidObj->id;
        }
        return 0;
    }
}
