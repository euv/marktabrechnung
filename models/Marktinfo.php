<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "marktinfo".
 *
 * @property int $id
 * @property int $markt_id
 * @property string $infosite
 * @property string|null $params
 *
 * @property Markt $markt
 */
class Marktinfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marktinfo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['markt_id', 'infosite'], 'required'],
            [['markt_id'], 'integer'],
            [['markt_id'], 'unique'],
            [['params'], 'string'],
            [['infosite'], 'string', 'max' => 255],
            [['markt_id'], 'exist', 'skipOnError' => true, 'targetClass' => Markt::class, 'targetAttribute' => ['markt_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'markt_id' => 'Markt ID',
            'infosite' => 'Infosite',
            'params' => 'Params',
        ];
    }

    /**
     * Gets query for [[Markt]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMarkt()
    {
        return $this->hasOne(Markt::class, ['id' => 'markt_id']);
    }

    public function getMarketlist()
    {
        // do not show deleted
        return ArrayHelper::map(Markt::find()->where(['not', ['status' => 3]])->all(), 'id', 'NameWithId');
    }

    public function get_markt_id(){
        return $this->markt->MarktID .' - '. $this->markt->Bezeichnung;
    }
}
