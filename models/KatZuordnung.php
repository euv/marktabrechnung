<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kat_zuordnung".
 *
 * @property int $id
 * @property int $KundenArt KundenArt
 * @property int $Markt Markt
 * @property int $Verbrauch Verbrauch
 * @property string $Kategorie Kategorie
 *
 * @property Markt $markt
 */

class KatZuordnung extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kat_zuordnung';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KundenArt', 'Markt', 'Verbrauch', 'Kategorie'], 'required'],
            [['KundenArt', 'Markt', 'Verbrauch'], 'integer'],
            [['Kategorie'], 'string', 'max' => 45],
            [['Markt'], 'exist', 'skipOnError' => true, 'targetClass' => Markt::class, 'targetAttribute' => ['Markt' => 'id']],
            [['KundenArt', 'Markt', 'Verbrauch'],'validateUnique', 'skipOnEmpty' => false, 'except' => 'delete'],
        ];
    }

    public function validateUnique($attribute, $params, $validator)
    {
        $test = KatZuordnung::find()->where([
            'KundenArt' => $this->KundenArt,
            'Markt' => $this->Markt,
            'Verbrauch' => $this->Verbrauch,
        ])->one();
        if ($test != null && $test->id != $this->id){
            $errorMsg = 'Diese Kombination wurde bereits mit Kategorie '.$test->Kategorie.' zugeordnet.';
            $this->addError($attribute, $errorMsg);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            '_KundenArt' => 'KundenArt',
            '_MarktIDAndName' => 'Markt',
            '_Verbrauch' => 'Verbrauch',
            'Kategorie' => 'Kategorie',
        ];
    }

    /**
     * Gets query for [[Markt]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMarkt()
    {
        return $this->hasOne(Markt::className(), ['id' => 'Markt']);
    }

    public function getMarketlist()
    {
        // show only actives
        return ArrayHelper::map(Markt::find()->where(['status' => 1])->all(), 'id', 'NameWithId');
    }

    public function getKategorieList()
    {
        return ArrayHelper::map(KatZuordnung::find()->all(), 'Kategorie', 'Kategorie');
    }

    public function get_KundenArt() {
        switch ($this->KundenArt) {
            case 0:
                $kundenArt = 'Tag';
                break;
            case 1:
                $kundenArt = 'Dauer';
                break;
            case 2:
                $kundenArt = 'Propagandist';
                break;
            default:
                $kundenArt = '';
        }
        return $kundenArt;
    }

    public function getKundenArtListe() {
        return ['0' => 'Tag', '1' => 'Dauer', '2' => 'Propagandist'];
    }

    public function get_MarktIDAndName()
    {
        return $this->markt->NameWithId;
    }

    public function getVerbrauchListe()
    {
        return ['0' => 'Meter', '1' => 'kWh'];
    }

    public function get_Verbrauch()
    {
        switch ($this->Verbrauch) {
            case 0:
                $verbrauch = 'Meter';
                break;
            case 1:
                $verbrauch = 'kWh';
                break;
            default:
                $verbrauch = '';
        }
        return $verbrauch;
    }
}
