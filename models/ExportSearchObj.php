<?php

namespace app\models;

use yii\data\ActiveDataProvider;

class ExportSearchObj extends Export
{
    public $debitorNr;
    public $objTitle;
    public $zusatzInfo;
    public $objBeginn;
     /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['debitorNr'], 'integer'],
            [['debitorNr', 'objTitle', 'zusatzInfo', 'objBeginn'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Export::find()
            ->where(['not', ['buchung.status' => 3]])
            ->andWhere(['buchung.isFirstBooking' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['debitor']);
        $query->joinWith(['markt']);
        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
                'debitorNr' => [
                    'asc' => ['LENGTH(DebitorNr)' => SORT_ASC, 'DebitorNr'=>SORT_ASC],
                    'desc' => ['LENGTH(DebitorNr)' => SORT_DESC, 'DebitorNr'=>SORT_DESC],
                    'label' => 'Adressnummer',
                    'default' => SORT_ASC
                ],
                'objTitle' => [
                    'asc' => ['kunde.Nachname' => SORT_ASC, 'kunde.Vorname' => SORT_ASC, 'markt.Bezeichnung' => SORT_ASC],
                    'desc' => ['kunde.Nachname' => SORT_DESC, 'kunde.Vorname' => SORT_DESC, 'markt.Bezeichnung' => SORT_DESC],
                    'label' => 'Objektbezeichnung',
                    'default' => SORT_ASC
                ],
                'zusatzInfo' => [
                    'asc' => ['LENGTH(markt.MarktID)' => SORT_ASC, 'markt.MarktID'=>SORT_ASC, 'LENGTH(DebitorNr)' => SORT_ASC, 'DebitorNr'=>SORT_ASC],
                    'desc' => ['LENGTH(markt.MarktID)' => SORT_DESC, 'markt.MarktID'=>SORT_DESC, 'LENGTH(DebitorNr)' => SORT_DESC, 'DebitorNr'=>SORT_DESC],
                    'label' => 'Zusatzinformation',
                    'default' => SORT_ASC
                ],
                'objBeginn' => [
                    'asc' => ['Datum'=>SORT_ASC],
                    'desc' => ['Datum'=>SORT_DESC],
                    'label' => 'Beginndatum',
                    'default' => SORT_ASC
                ],
            ],
            'defaultOrder' => ['objBeginn' => SORT_ASC],
        ]);

        if ($this->debitorNr != '') {
            $query->andFilterWhere(['or','kunde.DebitorNr LIKE "' . $this->debitorNr . '%"']);
        }

        if ($this->objTitle != '') {
            $query->andFilterWhere(['or','markt.Bezeichnung LIKE "%' . $this->objTitle . '%"']);
            $query->orFilterWhere(['or','kunde.Vorname LIKE "%' . $this->objTitle . '%" ', 'kunde.Nachname LIKE "%' . $this->objTitle . '%" ']);
        }

        if ($this->zusatzInfo != '') {
            $query->andFilterWhere(['or','markt.MarktId LIKE "'. $this->zusatzInfo . '%"']);
            $query->orFilterWhere(['or','kunde.DebitorNr LIKE "' . $this->zusatzInfo . '%"']);
        }

        if ($this->objBeginn != '') {
            $query->andFilterWhere(['like', 'Datum', $this->beginnReverse]);
        }

        return $dataProvider;
    }

    public function getBeginnReverse()
    {
        $time = explode(".", $this->objBeginn);
        $time = array_reverse($time);
        if (count($time) > 2){
            $time = [$time[0], $time[1]];
        }
        return implode("-", $time);
    }

}
