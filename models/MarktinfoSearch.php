<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Marktinfo;

/**
 * MarktinfoSearch represents the model behind the search form of `app\models\Marktinfo`.
 */
class MarktinfoSearch extends Marktinfo
{
    public $_markt_id;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'markt_id'], 'integer'],
            [['infosite', 'params', '_markt_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Marktinfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'markt_id' => $this->markt_id,
        ]);

        $query->andFilterWhere(['like', 'infosite', $this->infosite])
            ->andFilterWhere(['like', 'params', $this->params]);

        return $dataProvider;
    }
}
