<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * BuchungSearch represents the model behind the search form of `app\models\Buchung`.
 */
class BuchungSearch extends Buchung
{
    public $debitorIDAndName;
    public $marktIDAndName;
    public $_status;
    public $Datum;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'Debitor', 'MarktId', 'status'], 'integer'],
            [['Datum', 'created_at', 'updated_at', 'debitorIDAndName', 'marktIDAndName', '_status'], 'safe'],
            [['Meter', 'StromVerbrauch'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (Yii::$app->controller->action->id == 'index-filtered') {
            $query = Buchung::find()->where(['not', ['buchung.status' => 3]]);
        } else {
            $query = Buchung::find();
        }

        $query->joinWith(['debitor']);
        $query->joinWith(['markt']);

        if (isset($_GET['date']) && $_GET['date']){
            $this->Datum = $_GET['date'];
            $query->andFilterWhere([
                'Datum' => $this->Datum,
            ]);
        }

        if (isset($_GET['marktid']) && $_GET['marktid']){
            $this->marktIDAndName = $_GET['marktid'];
            $query->andFilterWhere([
                'buchung.MarktId' => $this->iDFromMarktID,
            ]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'debitorIDAndName' => [
                    'asc' => ['kunde.KundenID' => SORT_ASC, 'kunde.Vorname' => SORT_ASC, 'kunde.Nachname' => SORT_ASC],
                    'desc' => ['kunde.KundenID' => SORT_DESC, 'kunde.Vorname' => SORT_DESC, 'kunde.Nachname' => SORT_DESC],
                    'label' => 'KundenID',
                    'default' => SORT_ASC
                ],
                'marktIDAndName' => [
                    'asc' => ['markt.MarktID' => SORT_ASC, 'markt.Bezeichnung' => SORT_ASC],
                    'desc' => ['markt.MarktID' => SORT_DESC, 'markt.Bezeichnung' => SORT_DESC],
                    'label' => 'MarktID',
                    'default' => SORT_ASC
                ],
                'Datum',
                'Meter',
                'StromVerbrauch',
                '_status' => [
                    'asc' => ['buchung.status' => SORT_ASC],
                    'desc' => ['buchung.status' => SORT_DESC],
                    'label' => 'Status',
                    'default' => SORT_ASC
                ],
            ],
            'defaultOrder' => [ 'id' => SORT_DESC],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Meter' => $this->Meter,
            'StromVerbrauch' => $this->StromVerbrauch,
            'buchung.status' => $this->_status,
        ])
        ->andFilterWhere(['like', 'Datum', $this->Datum]);


        if ($this->debitorIDAndName != '') {
            $query->joinWith(['debitor' => function ($q) {
                $q->where(
                    'kunde.KundenID LIKE "' . $this->debitorIDAndName . '%"' .
                    'OR kunde.Vorname LIKE "%' . $this->debitorIDAndName . '%"' .
                    'OR kunde.Nachname LIKE "%' . $this->debitorIDAndName . '%"'
                );
            }]);
        }

        if ($this->marktIDAndName != '') {
            $query->joinWith(['markt' => function ($q) {
                $q->where([
                    'markt.id' => $this->marktIDAndName
                ]);
            }]);
        }

        return $dataProvider;
    }

    public function getMarktList() {
        return ArrayHelper::map(Markt::find()->orderBy(['MarktID'=>SORT_ASC])->all(), 'id', 'NameWithId');
    }

    public function getStatusList() {
        return [1 => 'aktiviert', 3 => 'storniert'];
    }


}
