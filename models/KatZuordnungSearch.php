<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KatZuordnung;

/**
 * KatZuordnungSearch represents the model behind the search form of `app\models\KatZuordnung`.
 */
class KatZuordnungSearch extends KatZuordnung
{

    public $_KundenArt;
    public $_MarktIDAndName;
    public $_Verbrauch;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'KundenArt', 'Markt', 'Verbrauch'], 'integer'],
            [['Kategorie', '_KundenArt', '_MarktIDAndName', '_Verbrauch'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KatZuordnung::find()->joinWith(['markt']);;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                '_MarktIDAndName' => SORT_ASC,
            ],
            'attributes' => [
                '_KundenArt'  => [
                    'asc' => ['KundenArt' => SORT_ASC],
                    'desc' => ['KundenArt' => SORT_DESC],
                    'label' => 'KundenArt',
                    'default' => SORT_ASC
                ],
                '_MarktIDAndName'  => [
                    'asc' => ['Markt' => SORT_ASC],
                    'desc' => ['Markt' => SORT_DESC],
                    'label' => 'Markt',
                    'default' => SORT_ASC
                ],
                '_Verbrauch'  => [
                    'asc' => ['Verbrauch' => SORT_ASC],
                    'desc' => ['Verbrauch' => SORT_DESC],
                    'label' => 'Verbrauch',
                    'default' => SORT_ASC
                ],
                'Kategorie' => [
                    'asc' => ['LENGTH(Kategorie)' => SORT_ASC, 'Kategorie'=>SORT_ASC],
                    'desc' => ['LENGTH(Kategorie)' => SORT_DESC, 'Kategorie'=>SORT_DESC],
                    'label' => 'Kategorie',
                    'default' => SORT_ASC
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Verbrauch' => $this->_Verbrauch,
            'KundenArt' => $this->_KundenArt,
            'markt.id' => $this->_MarktIDAndName,
            'Kategorie' => $this->Kategorie,
        ]);

        return $dataProvider;
    }

    public function get_KundenArtReverse() {
        switch ($this->_KundenArt) {
            case 'Tag':
                $kundenArt = 0;
                break;
            case 'Dauer':
                $kundenArt = 1;
                break;
            case 'Propagandist':
                $kundenArt = 2;
                break;
            default:
                $kundenArt = null;
        }
        return $kundenArt;
    }

}
