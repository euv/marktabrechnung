<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kunde;

/**
 * KundeSearch represents the model behind the search form of `app\models\Kunde`.
 */
class KundeSearch extends Kunde
{

    public $_NameVorname;
    public $_StrasseNr;
    public $_KundenArt;
    public $_status;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'KundenID', 'KundenArt', 'status'], 'integer'],
            [[
                'Vorname',
                'Nachname',
                'Straße',
                'Hausnummer',
                'PLZ',
                'Ort',
                'Land',
                'GewArt',
                'created_at',
                'modified_at',
                '_NameVorname',
                '_StrasseNr',
                '_KundenArt',
                'DebitorNr',
                '_status',
            ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (Yii::$app->controller->action->id == 'create-filtered'){
            $query = Kunde::find()->where(['status' => 1]); //do not show deleted or disabled ones
        } else {
            $query = Kunde::find()->where(['not', ['status' => 3]]); //do not show deleted ones
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                '_NameVorname' => SORT_ASC,
            ],
            'attributes' => [
                'KundenID',
                '_NameVorname' => [
                    'asc' => ['Nachname' => SORT_ASC, 'Vorname' => SORT_ASC],
                    'desc' => ['Nachname' => SORT_DESC, 'Vorname' => SORT_DESC],
                    'label' => 'Name, Vorname',
                    'default' => SORT_ASC
                ],
                '_StrasseNr' => [
                    'asc' => ['Straße' => SORT_ASC, 'Hausnummer' => SORT_ASC],
                    'desc' => ['Straße' => SORT_DESC, 'Hausnummer' => SORT_DESC],
                    'label' => 'Straße, Nr',
                    'default' => SORT_ASC
                ],
                'Ort',
                'GewArt',
                '_KundenArt'  => [
                    'asc' => ['KundenArt' => SORT_ASC],
                    'desc' => ['KundenArt' => SORT_DESC],
                    'label' => 'KundenArt',
                    'default' => SORT_ASC
                ],
                'DebitorNr',
                '_status'  => [
                    'asc' => ['Status' => SORT_ASC],
                    'desc' => ['Status' => SORT_DESC],
                    'label' => 'Status',
                    'default' => SORT_ASC
                ],
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'KundenID' => $this->KundenID,
        ]);

        $query->andFilterWhere(['like', 'Straße', $this->Straße])
            ->andFilterWhere(['like', 'Hausnummer', $this->Hausnummer])
            ->andFilterWhere(['like', 'Ort', $this->Ort])
            ->andFilterWhere(['like', 'GewArt', $this->GewArt]);

        if ($this->_NameVorname != '') {
            $query->andWhere('Vorname LIKE "%' . $this->_NameVorname . '%" ' .
                'OR Nachname LIKE "%' . $this->_NameVorname . '%"'
            );
        }

        if ($this->_StrasseNr != '') {
            $query->andWhere('Straße LIKE "%' . $this->_StrasseNr . '%" ' .
                'OR Hausnummer = "' . $this->_StrasseNr . '"'
            );
        }

        if ($this->DebitorNr != '') {
            $query->andWhere('DebitorNr LIKE "%' . $this->DebitorNr . '%" ');
        }

        $query->andFilterWhere([
            'KundenArt' => $this->_KundenArt,
        ]);

        if ($this->_status != '') {
            $query->andWhere([
                'Status' => $this->_status,
            ]);
        }

        return $dataProvider;
    }

    public function getKundenArtList(){
        return [0 => 'Tag', 1 => 'Dauer', 2 => 'Propagandist', ];
    }

    public function getStatusList(){
        return [0 => 'gesperrt', 1 => 'entsperrt'];
    }
}
