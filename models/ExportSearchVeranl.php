<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class ExportSearchVeranl extends Export
{
    public $zusatzInfo;
    public $menge;
    public $veranlBeginn;
    public $veranlEnde;


     /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['menge'], 'integer'],
            [['zusatzInfo', 'menge', 'veranlBeginn', 'veranlEnde', 'kategorie', 'isFirstBooking'], 'safe'],
        ]);
    }

    public function search($params)
    {
        $query = Export::find()->where(['not', ['buchung.status' => 3]])
        ->joinWith(['debitor'])
        ->joinWith(['markt']);

        $this->load($params);

        if ($this->zusatzInfo != '') {
            $query->andFilterWhere(['or','markt.MarktId LIKE "'. $this->zusatzInfo . '%"']);
            $query->orFilterWhere(['or','kunde.DebitorNr LIKE "' . $this->zusatzInfo . '%"']);
        }

        if ($this->menge != '') {

            $query->andWhere(['Meter'=>$this->menge]);
            $query->orWhere(['StromVerbrauch'=>$this->menge]);
        }

        if ($this->veranlBeginn != '') {
            $query->andFilterWhere(['like', 'Datum', $this->beginnReverse]);
        }

        if ($this->veranlEnde != '') {
            $query->andFilterWhere(['like', 'Datum', $this->endReverse]);
        }

        $models = $query->all();
        $addModels = [];

        if (count($models)) {
            foreach ($models as $model) {
                $kundenArt = $model->debitor->KundenArt;
                $marktId = $model->MarktId;
                if ($model->Meter != null && $model->StromVerbrauch == null) {
                    $verbrauch = 0;
                    $model->kategorie = $this->findKatZuordnung($kundenArt, $marktId, $verbrauch);

                    if (!$this->kategorie || $this->kategorie && $this->kategorie == $model->kategorie) {
                        $addModels[] = $model;
                    }
                }
                else if ($model->StromVerbrauch != null && $model->Meter == null) {
                    $verbrauch = 1;
                    $model->kategorie = $this->findKatZuordnung($kundenArt, $marktId, $verbrauch);
                    if (!$this->kategorie || $this->kategorie && $this->kategorie == $model->kategorie) {
                        $addModels[] = $model;
                    }
                } else if ($model->StromVerbrauch != null && $model->Meter != null) {
                    $doubleModel = new Export();
                    $doubleModel->attributes = $model->attributes;
                    $verbrauch = 0;
                    $model->kategorie = $this->findKatZuordnung($kundenArt, $marktId, $verbrauch);
                    $model->StromVerbrauch = null;
                    $verbrauch = 0;
                    $doubleModel->kategorie = $this->findKatZuordnung($kundenArt, $marktId, $verbrauch);
                    $doubleModel->Meter = null;
                    if ((!$this->menge || $this->menge && $this->menge == $model->Meter) &&
                        (!$this->kategorie || $this->kategorie && $this->kategorie == $model->kategorie)
                    ) {
                        $addModels[] = $model;
                    }
                    if ((!$this->menge || $this->menge && $this->menge == $doubleModel->StromVerbrauch) &&
                        (!$this->kategorie || $this->kategorie && $this->kategorie == $doubleModel->kategorie)
                    ) {
                        $addModels[] = $doubleModel;
                    }
                }
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' =>  $addModels,
            'sort' => [
                'attributes' => [
                    'zusatzInfo' => [
                        'asc' => ['LENGTH(markt.MarktID)' => SORT_ASC, 'markt.MarktID'=>SORT_ASC, 'LENGTH(DebitorNr)' => SORT_ASC, 'DebitorNr'=>SORT_ASC],
                        'desc' => ['LENGTH(markt.MarktID)' => SORT_DESC, 'markt.MarktID'=>SORT_DESC, 'LENGTH(DebitorNr)' => SORT_DESC, 'DebitorNr'=>SORT_DESC],
                        'label' => 'Zusatzinformation',
                        'default' => SORT_ASC
                    ],
                    'menge',
                    'veranlBeginn',
                    'veranlEnde' => [
                        'asc' => ['Datum' => SORT_ASC],
                        'desc' => ['Datum' => SORT_DESC],
                        'label' => 'Endedatum',
                        'default' => SORT_ASC
                    ],
                    'kategorie'
                ],
                'defaultOrder' => ['veranlBeginn' => SORT_ASC],
            ],
        ]);

        return $dataProvider;
    }

    public function getBeginnReverse()
    {
        $time = explode(".", $this->veranlBeginn);
        $time = array_reverse($time);
        return implode("-", $time);
    }

    public function getEndReverse()
    {
        $time = explode(".", $this->veranlEnde);
        $time = array_reverse($time);
        $timeImpl = implode("-", $time);
        if (count($time) > 2) {
            return date("Y-m-d", strtotime($timeImpl . " - 1 day"));
        }
        return $timeImpl;
    }

    public function getKategorieList()
    {
        return ArrayHelper::map(KatZuordnung::find()->orderBy(['Kategorie'=>SORT_ASC])->all(), 'Kategorie', 'Kategorie');
    }

}
