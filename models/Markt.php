<?php

namespace app\models;

use Yii;
use Yii\Helpers\Html;
use Yii\Helpers\Url;

/**
 * This is the model class for table "markt".
 *
 * @property int $id
 * @property string $Bezeichnung
 * @property string|null $MarktID
 * @property string|null $Markttage
 * @property string|null $Kostenstelle
 * @property string|null $Kostenträger
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int $status

 * @property Buchung[] $buchungs
 */
class Markt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $MarkttageCheckboxes = [];



    public static function tableName()
    {
        return 'markt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Bezeichnung', 'MarktID'], 'required'],
            [['created_at', 'updated_at', 'Markttage', 'MarkttageCheckboxes'], 'safe'],
            [['status'], 'integer'],
            [['Bezeichnung'], 'string', 'max' => 255],
            [['MarktID', 'Kostenstelle', 'Kostenträger'], 'string', 'max' => 45],
            [['Bezeichnung'], 'unique'],
            [['MarktID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Bezeichnung' => 'Bezeichnung',
            '_MarktID' => 'MarktID',
            'Markttage' => 'Markttage',
            '_Markttage' => 'Markttage',
            'Kostenstelle' => 'Kostenstelle',
            'Kostenträger' => 'Kostenträger',
            'created_at' => 'Erstellt am',
            'updated_at' => 'Geändert am',
            '_status' => 'Status',
        ];
    }

    /**
     * Gets query for [[Buchungs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBuchungs()
    {
        return $this->hasMany(Buchung::class, ['MarktId' => 'id']);
    }

    /**
     * Gets query for [[Marktinfos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMarktinfos()
    {
        return $this->hasOne(Marktinfo::class, ['markt_id' => 'id']);
    }

    public function getInfoButton(){
        if (isset($this->marktinfos, $this->marktinfos->id)) {
            $t = '/marktinfo/sites/?id='.$this->marktinfos->id;
            return Html::a('', Url::to($t), ['class' => 'glyphicon glyphicon-info-sign btn btn-default custom_button']);
        }
    }

    public function getMarkttageLabels()
    {
        return [
            1 => 'Mo',
            2 => 'Di',
            3 => 'Mi',
            4 => 'Do',
            5 => 'Fr',
            6 => 'Sa',
            7 => 'So',
        ];
    }

    public function get_Markttage()
    {
        if ($this->Markttage) {
            $daysArr = explode(',', $this->Markttage);
            $daysArrConverted = [];
            foreach ($daysArr as $dayNum) {
                $daysArrConverted[] = $this->getMarkttageLabels()[$dayNum];
            }
            return implode(',', $daysArrConverted);
        }
        return '';
    }

    public function get_Status()
    {
        return ($this->status == 1 ? 'aktiviert' : 'deaktiviert');
    }

    public function get_StatusReverse()
    {
        if ($this->_status == 'aktiviert'){
            return 1;
        } else if ($this->_status == 'deaktiviert'){
            return 0;
        }
        return null;
    }

    public function get_MarkttageReverse()
    {
        if ($pos = array_search(ucfirst($this->_Markttage), $this->MarkttageLabels, false)){
            return $pos;
        }
        return null;
    }

    public function get_MarktID(){
        return $this->MarktID;
    }

    public function getNameWithId()
    {
        return $this->MarktID .' - '. $this->Bezeichnung;
    }

    public function getButtonLabel()
    {
        return ($this->status == 1 ? $this->Bezeichnung : $this->Bezeichnung . ' (deaktiviert)');
    }

    public function getButtonStyle()
    {
        if (Yii::$app->user->can("fibu-management")) {
            return ($this->status == 1 ? 'markt btn btn-primary' : 'markt btn btn-default');
        } else {
            return ($this->status == 1 ? 'markt btn btn-primary' : 'markt btn btn-primary disabled');
        }
    }
}
