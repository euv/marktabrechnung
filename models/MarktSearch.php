<?php

namespace app\models;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * MarktSearch represents the model behind the search form of `app\models\Markt`.
 */
class MarktSearch extends Markt
{
    public $_Markttage;
    public $_MarktID;
    public $_status;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['Bezeichnung', '_MarktID', '_Markttage', '_status', 'Kostenstelle', 'Kostenträger', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Markt::find()->where(['not', ['status' => 3]]); //do not show deleted ones
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->setSort([
            'attributes' => [
                'Bezeichnung',
                '_MarktID',
                'Kostenstelle',
                'Kostenträger',
                '_status' => [
                    'asc' => ['status' => SORT_ASC],
                    'desc' => ['status' => SORT_DESC],
                    'label' => 'Status',
                    'default' => SORT_ASC
                ],
            ],
            'defaultOrder' => [ 'Bezeichnung' => SORT_ASC],
        ]);


        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->_status,
            'Kostenstelle' => $this->Kostenstelle,
            'Kostenträger' => $this->Kostenträger,
            'id' => $this->Bezeichnung,
        ]);

        $query->andFilterWhere(['id' => $this->_MarktID])
            ->andFilterWhere(['like', 'Markttage', $this->_MarkttageReverse]);

        echo $query->createCommand()->rawSql;
        return $dataProvider;
    }

    public function getMarktList(){
        return ArrayHelper::map($this::find()->orderBy(['MarktID'=>SORT_ASC])->all(), 'id', 'Bezeichnung');
    }

    public function getKostenstelleList(){
        return ArrayHelper::map($this::find()->orderBy(['Kostenstelle'=>SORT_ASC])->all(), 'Kostenstelle', 'Kostenstelle');
    }

    public function getKostenträgerList(){
        return ArrayHelper::map($this::find()->orderBy(['Kostenträger'=>SORT_ASC])->all(), 'Kostenträger', 'Kostenträger');
    }

    public function getMarktIDList(){
        return ArrayHelper::map($this::find()->orderBy(['MarktID'=>SORT_ASC])->all(), 'id', 'MarktID');
    }

    public function getStatusList(){
        return [0 => 'deaktiviert', 1 => 'aktiviert'];
    }
}
