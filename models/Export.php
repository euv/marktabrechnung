<?php

namespace app\models;


/**
 * This is the export for table "buchung".
 *
 *
 * @property Kunde $debitor
 * @property Markt $markt
 */
class Export extends Buchung
{
    public $kategorie;

    public function attributeLabels()
    {
        return [
            'debitorNr' => 'Adressnummer',
            'objTitle' => 'Objektbezeichnung',
            'zusatzInfo' => 'Zusatzinformation',
            'menge' => 'Menge',
            'objBeginn' => 'Beginndatum',
            'veranlBeginn' => 'Beginndatum',
            'veranlEnde' => 'Endedatum',
            'kategorie' => 'Kategorie',
        ];
    }

    public function getGermanDate1st()
    {
        $time = strtotime($this->veranlBeginn);
        return date('01.m.Y', $time);
    }

    public function getDebitorNr()
    {
        return $this->debitor->DebitorNr;
    }

    public function getObjTitle()
    {
        return $this->debitor->Nachname . " " . $this->debitor->Vorname . " - " . $this->markt->Bezeichnung;
    }

    public function getZusatzInfo()
    {
        return $this->markt->MarktID . "-" . $this->debitorNr;
    }

    public function getObjBeginn()
    {
        return $this->getGermanDate1st();
    }

    public function getVeranlBeginn()
    {
        return $this->getGermanDate();
    }

    public function getVeranlEnde()
    {
        return date("d.m.Y", strtotime($this->veranlBeginn . " + 1 day"));
    }

    public function getMenge()
    {
        return ($this->Meter != null ? $this->Meter : $this->StromVerbrauch);
    }

    public function findKatZuordnung($kundenArt, $markt, $verbrauch)
    {
        $find = KatZuordnung::find()->where([
            'KundenArt' => $kundenArt,
            'Markt' => $markt,
            'Verbrauch' => $verbrauch
        ])->one();
        if ($find != null) {
            return $find->Kategorie;
        }
        return null;
    }

}
