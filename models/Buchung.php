<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "buchung".
 *
 * @property int $id
 * @property int $Debitor
 * @property int $MarktId
 * @property string $Datum
 * @property float|null $Meter
 * @property float|null $StromVerbrauch
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int $status
 *
 * @property Kunde $debitor
 * @property Markt $markt
 */
class Buchung extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'buchung';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Debitor', 'MarktId', 'Datum'], 'required'],
            [['Debitor', 'MarktId', 'status'], 'integer'],
            [['Datum', 'created_at', 'updated_at'], 'safe'],
            [['Meter', 'StromVerbrauch'], 'number'],
            [['Meter', 'StromVerbrauch'],'validateMeterOrStrom', 'skipOnEmpty' => false, 'except' => 'delete'],
            [['Debitor'], 'exist', 'skipOnError' => true, 'targetClass' => Kunde::class, 'targetAttribute' => ['Debitor' => 'id']],
            [['MarktId'], 'exist', 'skipOnError' => true, 'targetClass' => Markt::class, 'targetAttribute' => ['MarktId' => 'id']],
        ];
    }

    public function validateMeterOrStrom($attribute, $params, $validator)
    {
        if ($this->Meter=='' && $this->StromVerbrauch=='') {
            $errorMsg = 'Eines dieser Felder muss ausgefüllt sein.';
            $this->addError($attribute, $errorMsg);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Debitor' => 'KundenID',
            'MarktId' => 'MarktID',
            'Datum' => 'Datum',
            'Meter' => 'Meter',
            'StromVerbrauch' => 'Stromverbrauch (kW)',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            '_status' => 'Status',
            'debitorIDAndName' => 'KundenID',
            'marktIDAndName' => 'MarktID'
        ];
    }

    /**
     * Gets query for [[Debitor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDebitor()
    {
        return $this->hasOne(Kunde::class, ['id' => 'Debitor']);
    }

    public function getDebitorIDAndName()
    {
        return $this->debitor->NameWithKdNr;
    }

    public function getDebitorlist()
    {
        // show only actives
        return ArrayHelper::map(Kunde::find()->where(['status' => 1])->all(), 'id', 'NameWithKdNr');
    }

    /**
     * Gets query for [[Markt]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMarkt()
    {
        return $this->hasOne(Markt::class, ['id' => 'MarktId']);
    }

    public function getMarktIDAndName()
    {
        return $this->markt->NameWithId;
    }

    public function getIDFromMarktID(){
        $marktId = $_GET['marktid'];
        if ($markt = Markt::find()->where(['MarktID' => $marktId])->one()) {
            return $markt->id;
        }
        return null;
    }

    public function getMarketlist()
    {
        // show only actives
        return ArrayHelper::map(Markt::find()->where(['status' => 1])->all(), 'id', 'NameWithId');
    }

    public function getGermanDate(){
        $time = strtotime($this->Datum);
        return date('d.m.Y', $time);
    }

    public function getLockstatus()
    {
        switch ($this->status) {
            case 0:
                return "deaktiviert";
            case 1:
                return "aktiviert";
            case 3:
                return "storniert";
            default:
                return "";
        }
    }

    public function get_status(){
        return $this->lockstatus;
    }

}
